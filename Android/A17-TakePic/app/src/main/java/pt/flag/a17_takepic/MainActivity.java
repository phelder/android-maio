package pt.flag.a17_takepic;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_CAMERA = 0;
    private final int REQUEST_GALLERY = 1;

    private ImageView ivPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);

        Button btnMudaImg = (Button) findViewById(R.id.btnMudaImg);
        btnMudaImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ivPhoto.setImageResource(R.drawable.homer_simpson);

            }
        });

        Button btnTiraFoto = (Button) findViewById(R.id.btnTiraFoto);
        btnTiraFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                startActivityForResult(i, REQUEST_CAMERA);
            }
        });

        Button btnEscolheFoto = (Button) findViewById(R.id.btnEscolheFoto);
        btnEscolheFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");

                startActivityForResult(i, REQUEST_GALLERY);
            }
        });

        Button btnEscreveFicheiro = (Button) findViewById(R.id.btnEscreveFicheiro);
        btnEscreveFicheiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                escreveFicheiro();
            }
        });

        Button btnLeFicheiro = (Button) findViewById(R.id.btnLeFicheiro);
        btnLeFicheiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leFicheiro();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("camara", "ReqC: " + requestCode);
        Log.i("camara", "ResC: " + resultCode);
        Log.i("camara", "Data: " + data.getExtras().toString());

        Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");

        ivPhoto.setImageBitmap(imageBitmap);
    }

    private void escreveFicheiro() {
        // Preciso do caminho para o internal storage (pasta de docs da app)
        // preciso de concatenar o nome do ficheiro de imagem
        // preciso de criar o output stream com esta info

        Bitmap imageBitmap = ((BitmapDrawable)ivPhoto.getDrawable()).getBitmap();

        String newFilePath = getFilesDir().toString() + "/img1.jpg";

        try {

            FileOutputStream fos = new FileOutputStream(newFilePath);

            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void leFicheiro() {

        String filePath = getFilesDir().toString() + "/img1.jpg";

        Bitmap bmp = BitmapFactory.decodeFile(filePath);

        ivPhoto.setImageBitmap(bmp);

    }
}
