package pt.flag.a21_picasso;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final View root = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);

        Snackbar snackbar = Snackbar.make(root, "TESTE SNACKBAR", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadImage();
            }
        });
        snackbar.show();
    }

    private void downloadImage() {

        ImageView ivSomething = (ImageView) findViewById(R.id.ivSomething);

        Picasso.with(this).load("http://battleroyalewithcheese.com/wp-content/uploads/2015/06/Homer_simpsonwoohooo.gif")
                .placeholder(android.R.drawable.btn_dropdown)
                .into(ivSomething);

    }
}
