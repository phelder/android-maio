package pt.flag.a9_bloconotas;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int VIEW_OPTION = 0;
    private static final int EDIT_OPTION = 1;
    private static final int DELETE_OPTION = 2;

    private ArrayAdapter<Note> noteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NotesDataSource.notes = new ArrayList<>();

        NotesDataSource.notes.add(new Note("dummy title 1", "dummy desc 1"));
        NotesDataSource.notes.add(new Note("dummy title 2", "dummy desc 2"));
        NotesDataSource.notes.add(new Note("dummy title 3", "dummy desc 3"));

        Button btnNewNote = (Button) findViewById(R.id.btnNewNote);
        ListView lvNotes = (ListView) findViewById(R.id.lvNotes);

        noteAdapter = new ArrayAdapter<Note>(this, android.R.layout.simple_list_item_1, NotesDataSource.notes);

        lvNotes.setAdapter(noteAdapter);

        lvNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoViewNote(position);
            }
        });

        btnNewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoEditNote(-1);
            }
        });

        // REGISTA PARA O MENU DE CONTEXTO
        // QUANDO FAZEMOS LONG CLICK DISPARA O METODO ONCREATECONTEXTMENU
        registerForContextMenu(lvNotes);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // REFRESH A LISTVIEW
        noteAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderIcon(android.R.drawable.ic_dialog_info);
        menu.setHeaderTitle("Escolha");

        menu.add(0, VIEW_OPTION, 0, "View");
        menu.add(0, EDIT_OPTION, 1, "Edit");
        menu.add(0, DELETE_OPTION, 2, "Delete");

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int pos = ((AdapterView.AdapterContextMenuInfo)
                item.getMenuInfo()).position;

        if (item.getItemId() == VIEW_OPTION) {
            gotoViewNote(pos);
        }

        if (item.getItemId() == EDIT_OPTION) {
            gotoEditNote(pos);
        }

        if (item.getItemId() == DELETE_OPTION) {
            deleteNote(pos);
        }

        return true;
    }

    private void gotoViewNote(int position) {

        Intent i = new Intent(this, ViewNoteActivity.class);

        i.putExtra("pos", position);

        startActivity(i);

    }

    private void gotoEditNote(int position) {

        Intent i = new Intent(this, EditNoteActivity.class);

        i.putExtra("pos", position);

        startActivity(i);

    }

    private void deleteNote(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Apagar");
        builder.setIcon(android.R.drawable.ic_delete);
        builder.setMessage("Tem a certeza?");

        builder.setNegativeButton("Não", null);
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                NotesDataSource.notes.remove(position);

                noteAdapter.notifyDataSetChanged();
            }
        });

        AlertDialog confirm = builder.create();

        confirm.show();
    }
}
