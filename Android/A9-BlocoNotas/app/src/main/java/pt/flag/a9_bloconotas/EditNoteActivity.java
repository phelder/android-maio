package pt.flag.a9_bloconotas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditNoteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        final EditText etTitle = (EditText) findViewById(R.id.etTitle);
        final EditText etDescription = (EditText) findViewById(R.id.etDescription);
        Button btnSave = (Button) findViewById(R.id.btnSave);


        final int pos = getIntent().getIntExtra("pos", -1);

        if (pos >= 0) {
            Note note = NotesDataSource.notes.get(pos);

            etTitle.setText(note.getTitle());
            etDescription.setText(note.getDescription());
        }


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = etTitle.getText().toString();
                String description = etDescription.getText().toString();

                if (pos >= 0) {
                    // VAMOS EDITAR
                    Note note = NotesDataSource.notes.get(pos);

                    note.setTitle(title);
                    note.setDescription(description);

                } else {
                    // VAMOS CRIAR UM NOVO
                    Note newNote = new Note(title, description);

                    NotesDataSource.notes.add(newNote);
                }



                finish();
            }
        });
    }
}
