package pt.flag.a9_bloconotas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ViewNoteActivity extends AppCompatActivity {

    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_note);

        pos = getIntent().getIntExtra("pos", -1);

        Button btnEdit = (Button) findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ViewNoteActivity.this, EditNoteActivity.class);

                i.putExtra("pos", pos);

                startActivity(i);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) findViewById(R.id.tvDescription);

        if (pos >= 0) {
            Note note = NotesDataSource.notes.get(pos);

            tvTitle.setText(note.getTitle());
            tvDescription.setText(note.getDescription());

        } else {
            Toast.makeText(this, "Não sei como chegaste aqui...", Toast.LENGTH_LONG).show();
        }
    }
}
