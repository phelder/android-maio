package pt.flag.a23_viewpager;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class SongPageFragment extends Fragment {

    private Song song;

    private TextView tvTitle;
    private TextView tvArtist;
    private ImageView ivThumb;

    public SongPageFragment() {
        // Required empty public constructor
    }

    public void setSong(Song song) {
        this.song = song;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_song_page, container, false);

        tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        tvArtist = (TextView) v.findViewById(R.id.tvArtist);
        ivThumb = (ImageView) v.findViewById(R.id.ivThumb);

        tvTitle.setText(song.getTitle());
        tvArtist.setText(song.getArtist());

        Picasso.with(getActivity()).load(song.getThumbURL()).into(ivThumb);

        return v;
    }

}
