package pt.flag.a23_viewpager;

/**
 * Created by helder on 19/07/16.
 */
public class Song {
    private int id;
    private String title;
    private String artist;
    private String duration;
    private String thumbURL;
    private String lyrics;

    public Song() {
    }

    public Song(int id, String title, String artist, String duration, String thumbURL, String lyrics) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.duration = duration;
        this.thumbURL = thumbURL;
        this.lyrics = lyrics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getThumbURL() {
        return thumbURL;
    }

    public void setThumbURL(String thumbURL) {
        this.thumbURL = thumbURL;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }
}
