package pt.flag.a23_viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by helder on 19/07/16.
 */
public class SongPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Song> songs;

    public SongPagerAdapter(FragmentManager fm, ArrayList<Song> songs) {
        super(fm);
        this.songs = songs;
    }

    @Override
    public Fragment getItem(int position) {

        Song s = songs.get(position);

        SongPageFragment spf = new SongPageFragment();

        spf.setSong(s);

        return spf;
    }

    @Override
    public int getCount() {
        return songs.size();
    }
}
