package pt.flag.a23_viewpager;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyInternetTask mit = new MyInternetTask("http://reality6.com/musicservicejson.php");
        mit.setOnConnectionListener(new MyInternetTask.OnConnectionListener() {
            @Override
            public void onConnectionSuccess(String result) {
                parseResult(result);
            }

            @Override
            public void onConnectionFailure() {

            }
        });
        mit.execute();

    }

    private void parseResult(String result) {

        ArrayList<Song> songs = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(result);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject object = (JSONObject) jsonArray.get(i);

                Song s = new Song();
                s.setId(object.getInt("id"));
                s.setArtist(object.getString("artist"));
                s.setTitle(object.getString("title"));
                s.setDuration(object.getString("duration"));
                s.setLyrics(object.getString("lyrics"));
                s.setThumbURL(object.getString("thumb_url"));

                songs.add(s);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        preencheViewPager(songs);

    }

    private void preencheViewPager(ArrayList<Song> songs) {

        SongPagerAdapter spa = new SongPagerAdapter(getSupportFragmentManager(), songs);

        ViewPager vpSongs = (ViewPager) findViewById(R.id.vpSongs);
        vpSongs.setAdapter(spa);

    }
}
