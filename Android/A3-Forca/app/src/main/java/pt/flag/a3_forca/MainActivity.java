package pt.flag.a3_forca;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Button> disabledButtons;
    private Forca forca;

    private TextView tvFraseJogo;
    private TextView tvVidas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvFraseJogo = (TextView) findViewById(R.id.tvFraseJogo);
        tvVidas = (TextView) findViewById(R.id.tvVidas);

        iniciaJogo();
    }

    private void iniciaJogo() {
        String[] frases = {
                "Gotham City",
                "Metropolis",
                "Star City",
                "Coast City",
                "Atlantis",
                "New Themyscira",
                "Central City"
        };

        forca = new Forca(frases);

        tvFraseJogo.setText(forca.getFraseJogo());
        tvVidas.setText("Vidas: " + forca.getVidas());

        // FAZ RESET AO ENABLE DOS BOTOES

        if (disabledButtons != null) {
            for (int i = 0; i < disabledButtons.size(); i++) {
                disabledButtons.get(i).setEnabled(true);
            }
        }
        disabledButtons = new ArrayList<>();


//        LinearLayout llButtonContainer = (LinearLayout) findViewById(R.id.llButtonContainer);
//        enableView(llButtonContainer);




//        LinearLayout llButtonContainer = (LinearLayout) findViewById(R.id.llButtonContainer);
//
//        for (int i = 0; i < llButtonContainer.getChildCount(); i++) {
//
//            LinearLayout linha = (LinearLayout) llButtonContainer.getChildAt(i);
//            for (int j = 0; j < linha.getChildCount(); j++) {
//                Button b = (Button) linha.getChildAt(i);
//                b.setEnabled(true);
//            }
//
//        }






//        LinearLayout llLinha1 = (LinearLayout) findViewById(R.id.llLinha1);
//        for (int i = 0; i < llLinha1.getChildCount(); i++) {
//            Button b = (Button) llLinha1.getChildAt(i);
//            b.setEnabled(true);
//        }
//        LinearLayout llLinha2 = (LinearLayout) findViewById(R.id.llLinha2);
//        for (int i = 0; i < llLinha2.getChildCount(); i++) {
//            Button b = (Button) llLinha2.getChildAt(i);
//            b.setEnabled(true);
//        }
//        LinearLayout llLinha3 = (LinearLayout) findViewById(R.id.llLinha3);
//        for (int i = 0; i < llLinha3.getChildCount(); i++) {
//            Button b = (Button) llLinha3.getChildAt(i);
//            b.setEnabled(true);
//        }
//        LinearLayout llLinha4 = (LinearLayout) findViewById(R.id.llLinha4);
//        for (int i = 0; i < llLinha4.getChildCount(); i++) {
//            Button b = (Button) llLinha4.getChildAt(i);
//            b.setEnabled(true);
//        }
    }

    public void clickedLetter(View v) {

        if (!forca.emJogo()) {
            Toast.makeText(this, "O JOGO JA ACABOU...", Toast.LENGTH_SHORT).show();
            return;
        }

        Button button = (Button) v;

        button.setEnabled(false);
        disabledButtons.add(button);

        Toast.makeText(this, button.getText(), Toast.LENGTH_SHORT).show();

        char letra = button.getText().charAt(0);

        forca.verificaLetra(letra);

        tvFraseJogo.setText(forca.getFraseJogo());
        tvVidas.setText("Vidas: " + forca.getVidas());

        if (!forca.emJogo()) {
            if (forca.ganhou()) {
                Toast.makeText(this, "YAY!!!!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "NABO!!!!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void enableView(View v) {

        if (v instanceof Button) {
            Button b = (Button)v;
            b.setEnabled(true);
        } else {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup)v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    enableView(vg.getChildAt(i));
                }
            }
        }

    }

    public void clickedNovoJogo(View v) {

        iniciaJogo();

    }
}
