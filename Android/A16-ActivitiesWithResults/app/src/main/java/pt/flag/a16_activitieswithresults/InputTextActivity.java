package pt.flag.a16_activitieswithresults;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

public class InputTextActivity extends AppCompatActivity {

    private EditText etInputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_text);

        etInputText = (EditText) findViewById(R.id.etInputText);

    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("text1", etInputText.getText().toString());

        setResult(RESULT_OK, resultIntent);

        finish();
    }
}
