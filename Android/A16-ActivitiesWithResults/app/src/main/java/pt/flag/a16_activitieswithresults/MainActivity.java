package pt.flag.a16_activitieswithresults;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_INPUT_TEXT = 0;

    private TextView tvText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvText = (TextView) findViewById(R.id.tvText);

        Button btnGo = (Button) findViewById(R.id.btnGo);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, InputTextActivity.class);

                startActivityForResult(i, REQUEST_INPUT_TEXT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
// SO TENHO UM REQUEST E UMA RESPOSTA NAO E NECESSARIO VERIFICAR
//        if (requestCode == REQUEST_INPUT_TEXT) {
//
//            if (resultCode == RESULT_OK) {
//
//            }
//
//        }


        String resposta = data.getStringExtra("text1");

        tvText.setText(resposta);

//        Toast.makeText(this, "YAY RESPOSTA", Toast.LENGTH_LONG).show();

    }
}
