package pt.flag.a18_listaamigos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by helder on 08/07/16.
 */
public class MyHelper extends SQLiteOpenHelper {

    public MyHelper(Context context) {
        super(context, "db_amigos.sqlite", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTableAmigos = "CREATE TABLE amigos (" +
                "_id INTEGER PRIMARY KEY, " +
                "nome TEXT, " +
                "morada TEXT, " +
                "data_nascimento INTEGER, " +
                "foto TEXT)";

        db.execSQL(createTableAmigos);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
