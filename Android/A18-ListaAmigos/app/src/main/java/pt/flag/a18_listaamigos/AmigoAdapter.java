package pt.flag.a18_listaamigos;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by helder on 08/07/16.
 */
public class AmigoAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Amigo> amigos;

    public AmigoAdapter(Context context, ArrayList<Amigo> amigos) {
        this.context = context;
        this.amigos = amigos;
    }

    @Override
    public int getCount() {
        return amigos.size();
    }

    @Override
    public Object getItem(int position) {
        return amigos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return amigos.get(position).get_id();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.list_item_amigo, parent, false);

        ImageView ivFoto = (ImageView) v.findViewById(R.id.ivFoto);
        TextView tvNome = (TextView) v.findViewById(R.id.tvNome);

        Amigo a = amigos.get(position);

        tvNome.setText(a.getNome());

        // VAMOS LER A IMAGEM...

        String filePath = context.getFilesDir() + "/" + a.getFoto();

        Bitmap bmp = BitmapFactory.decodeFile(filePath);

        ivFoto.setImageBitmap(bmp);

        return v;
    }
}
