package pt.flag.a18_listaamigos;

import java.util.Date;

/**
 * Created by helder on 08/07/16.
 */
public class Amigo {

    private long _id;
    private String nome;
    private String morada;
    private long dataNascimento;
    private String foto;

    public Amigo(long _id, String nome, String morada, long dataNascimento, String foto) {
        this._id = _id;
        this.nome = nome;
        this.morada = morada;
        this.dataNascimento = dataNascimento;
        this.foto = foto;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public long getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(long dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
