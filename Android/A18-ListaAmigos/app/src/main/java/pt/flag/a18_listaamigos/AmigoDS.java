package pt.flag.a18_listaamigos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by helder on 08/07/16.
 */
public class AmigoDS {

    private SQLiteDatabase db;

    public AmigoDS(SQLiteDatabase db) {
        this.db = db;
    }

    public long insereAmigo(String nome, String morada, long dataNascimento, String foto) {

        ContentValues values = new ContentValues();
        values.put("nome", nome);
        values.put("morada", morada);
        values.put("data_nascimento", dataNascimento);
        values.put("foto", foto);

        return db.insert("amigos", null, values);

    }

    public void actualizaAmigo(long id, String nome, String morada, long dataNascimento, String foto) {

        ContentValues values = new ContentValues();
        values.put("nome", nome);
        values.put("morada", morada);
        values.put("data_nascimento", dataNascimento);
        values.put("foto", foto);

        db.update("amigos", values, "_id = " + id, null);

    }

    public ArrayList<Amigo> listaAmigos() {

        ArrayList<Amigo> amigos = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM amigos", null);

        if (cursor.getCount() == 0) {
            return amigos;
        }

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            long _id = cursor.getLong(0);
            String nome = cursor.getString(1);
            String morada = cursor.getString(2);
            long dataNascimento = cursor.getLong(3);
            String foto = cursor.getString(4);

            Amigo a = new Amigo(_id, nome, morada, dataNascimento, foto);

            amigos.add(a);

            cursor.moveToNext();
        }

        return amigos;

    }

    public Amigo amigoComId(long id) {

        Cursor cursor = db.rawQuery("SELECT * FROM amigos WHERE _id = " + id, null);

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        long _id = cursor.getLong(0);
        String nome = cursor.getString(1);
        String morada = cursor.getString(2);
        long dataNascimento = cursor.getLong(3);
        String foto = cursor.getString(4);

        return new Amigo(_id, nome, morada, dataNascimento, foto);

    }
}
