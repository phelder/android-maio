package pt.flag.a18_listaamigos;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EditarAmigoActivity extends AppCompatActivity {

    private Amigo amigo = null;

    private final int REQUEST_CAMERA = 0;
    private final int REQUEST_GALLERY = 1;

    private Date currentShownDate;


    private TextView tvDataNascimento;
    private Button btnEscolheData;
    private Button btnTiraFoto;
    private Button btnEscolheFoto;
    private EditText etNome;
    private EditText etMorada;
    private ImageView ivFoto;
    private Button btnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_amigo);

        tvDataNascimento = (TextView) findViewById(R.id.tvDataNascimento);
        btnEscolheData = (Button) findViewById(R.id.btnEscolheData);
        btnTiraFoto = (Button) findViewById(R.id.btnTiraFoto);
        btnEscolheFoto = (Button) findViewById(R.id.btnEscolheFoto);
        ivFoto = (ImageView) findViewById(R.id.ivFoto);
        etNome = (EditText) findViewById(R.id.etNome);
        etMorada = (EditText) findViewById(R.id.etMorada);
        btnSave = (Button) findViewById(R.id.btnSave);

        // LER DA BD
        long id = getIntent().getLongExtra("id", -1);
        if (id > 0) {

            MyHelper myHelper = new MyHelper(this);
            SQLiteDatabase db = myHelper.getWritableDatabase();
            AmigoDS ads = new AmigoDS(db);

            amigo = ads.amigoComId(id);

            db.close();
            myHelper.close();

            etNome.setText(amigo.getNome());
            etMorada.setText(amigo.getMorada());

            currentShownDate = new Date(amigo.getDataNascimento());
            showFormatedDate();

            // VAMOS LER A IMAGEM...

            String filePath = getFilesDir() + "/" + amigo.getFoto();
            Bitmap bmp = BitmapFactory.decodeFile(filePath);
            ivFoto.setImageBitmap(bmp);

        } else {

            currentShownDate = new Date();
            showFormatedDate();

        }


        btnEscolheData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                escolherData();
            }
        });

        btnTiraFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                startActivityForResult(i, REQUEST_CAMERA);

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarAmigo();
            }
        });
    }

    private void escolherData() {

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(currentShownDate);

        DatePickerDialog dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

//                String dateText = year + " / " + (monthOfYear + 1) + " / " + dayOfMonth;

                GregorianCalendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                currentShownDate = cal.getTime();

                showFormatedDate();


//                SimpleDateFormat sdf = new SimpleDateFormat("yy/M/d");
//                tvDataNascimento.setText(sdf.format(cal.getTime()));



            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        dpd.show();
    }

    private void showFormatedDate() {

        DateFormat df = android.text.format.DateFormat.getDateFormat(EditarAmigoActivity.this);

        String textDate = df.format(currentShownDate);

        tvDataNascimento.setText(textDate);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");

            ivFoto.setImageBitmap(imageBitmap);
        }

    }

    private void guardarAmigo() {

        String nome = etNome.getText().toString();
        String morada = etMorada.getText().toString();
        long dataNascimento = currentShownDate.getTime();
        Bitmap imageBitmap = ((BitmapDrawable)ivFoto.getDrawable()).getBitmap();

        String fotoFilename; // NOME DO FX PARA GUARDAR NA BD
        String filePath; // CAMINHO COMPLETO PRA GUARDAR O FX NO DISCO


        if (amigo == null) { // VAMOS CRIAR UM NOVO NOME PARA O FX

            long currentTimeStamp = new Date().getTime();
            fotoFilename = currentTimeStamp + ".png";


        } else { // VAMOS USAR O NOME JA EXISTENTE

            fotoFilename = amigo.getFoto();

        }

        filePath = getFilesDir().toString() + "/" + fotoFilename;

        // VAMOS GUARDAR A IMAGEM NO DISCO
        try {
            FileOutputStream fos = new FileOutputStream(filePath);

            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



        MyHelper myHelper = new MyHelper(this);
        SQLiteDatabase db = myHelper.getWritableDatabase();

        AmigoDS ads = new AmigoDS(db);

        if (amigo == null) { // VAMOS INSERIR

            long id = ads.insereAmigo(nome, morada, dataNascimento, fotoFilename);

            Toast.makeText(this, "Amigo inserido com o id " + id, Toast.LENGTH_LONG).show();
        } else { // VAMOS ACTUALIZAR

            ads.actualizaAmigo(amigo.get_id(), nome, morada, dataNascimento, fotoFilename);

            Toast.makeText(this, "Actualizado", Toast.LENGTH_LONG).show();
        }

        db.close();
        myHelper.close();

        finish();

    }
}
