package pt.flag.a18_listaamigos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView lvAmigos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvAmigos = (ListView) findViewById(R.id.lvAmigos);

        Button btnAdicionaAmigo = (Button) findViewById(R.id.btnAdicionaAmigo);
        btnAdicionaAmigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoEditarAmigo(-1);
            }
        });

        lvAmigos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoEditarAmigo(id);
            }
        });
    }

    private void gotoEditarAmigo(long id) {
        Intent i = new Intent(MainActivity.this, EditarAmigoActivity.class);

        i.putExtra("id", id);

        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyHelper myHelper = new MyHelper(this);
        AmigoDS ads = new AmigoDS(myHelper.getWritableDatabase());

        ArrayList<Amigo> amigos = ads.listaAmigos();

        myHelper.close();


        AmigoAdapter adapter = new AmigoAdapter(this, amigos);

        lvAmigos.setAdapter(adapter);
    }
}
