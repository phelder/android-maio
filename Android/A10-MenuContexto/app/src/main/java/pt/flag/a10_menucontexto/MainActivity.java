package pt.flag.a10_menucontexto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn1 = (Button) findViewById(R.id.btn1);
        Button btn2 = (Button) findViewById(R.id.btn2);

        registerForContextMenu(btn1);
        registerForContextMenu(btn2);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.btn1) {
            menu.setHeaderTitle("ESCOLHA::::");
            menu.setHeaderIcon(android.R.drawable.ic_menu_agenda);

            menu.add(0, 0, 0, "OPTION 1");
            menu.add(0, 1, 1, "OPTION 2");
            menu.add(0, 2, 2, "OPTION 3");
            menu.add(0, 3, 3, "OPTION 4");
            menu.add(0, 4, 4, "OPTION 5");
            menu.add(0, 5, 5, "OPTION 6");
        }

        if (v.getId() == R.id.btn2) {
            menu.setHeaderTitle("ESCOLHA OUTRO");
            menu.setHeaderIcon(android.R.drawable.ic_lock_silent_mode);

            menu.add(1, 0, 0, "OPTION 10");
            menu.add(1, 1, 1, "OPTION 20");

        }


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (item.getGroupId() == 0) {

            if (item.getItemId() == 0) {
                Toast.makeText(this, "YAY 1º do 1º", Toast.LENGTH_SHORT).show();
            }

        }

        if (item.getGroupId() == 1) {
            
        }



        return true;
    }
}
