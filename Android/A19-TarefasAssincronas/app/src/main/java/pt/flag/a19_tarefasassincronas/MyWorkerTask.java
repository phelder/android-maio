package pt.flag.a19_tarefasassincronas;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by helder on 11/07/16.
 */

// Tipos Genericos: Params, Progress, Result
public class MyWorkerTask extends AsyncTask<Integer, Integer, String> {

    public interface MyWorkerTaskListener {
        void onTaskStart();
        void onTaskProgress(int progress);
        void onTaskResult(String result);
    }

    private MyWorkerTaskListener listener;

    public void setMyWorkerTaskListener(MyWorkerTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) {
            listener.onTaskStart();
        }
    }

    @Override
    protected String doInBackground(Integer... params) {

        for (int i = 0; i < 100; i++) {

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            publishProgress(i + 1);

        }

        return "ESTE E O RESULTADO...";
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (listener != null) {
            listener.onTaskProgress(values[0]);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if (listener != null) {
            listener.onTaskResult(result);
        }
    }
}
