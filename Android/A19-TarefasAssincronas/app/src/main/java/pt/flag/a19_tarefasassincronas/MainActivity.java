package pt.flag.a19_tarefasassincronas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ProgressBar progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);
        final TextView tvText1 = (TextView) findViewById(R.id.tvText1);


        MyWorkerTask mwt = new MyWorkerTask();

        mwt.setMyWorkerTaskListener(new MyWorkerTask.MyWorkerTaskListener() {
            @Override
            public void onTaskStart() {
                tvText1.setText("A TRABALHAR...");
            }

            @Override
            public void onTaskProgress(int progress) {
                progressBar1.setProgress(progress);
            }

            @Override
            public void onTaskResult(String result) {
                Toast.makeText(MainActivity.this, result, Toast.LENGTH_LONG).show();
                tvText1.setText("FIM... RES NA TOSTA");
            }
        });

        mwt.execute();




    }
}
