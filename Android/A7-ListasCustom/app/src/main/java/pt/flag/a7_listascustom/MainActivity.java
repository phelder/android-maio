package pt.flag.a7_listascustom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Pessoa> malta = new ArrayList<>();

        malta.add(new Pessoa("Tobias", 20, "Porto"));
        malta.add(new Pessoa("Tomé", 21, "Lisboa"));
        malta.add(new Pessoa("Zeca", 22, "Lisboa"));
        malta.add(new Pessoa("Boné", 40, "Porto"));
        malta.add(new Pessoa("Ruben", 30, "Aveiro"));
        malta.add(new Pessoa("Popeye", 25, "Braga"));
        malta.add(new Pessoa("Margarida", 19, "Coimbra"));
        malta.add(new Pessoa("Sofia", 30, "Porto"));
        malta.add(new Pessoa("Olivia Palito", 30, "Porto"));
        malta.add(new Pessoa("Brutus", 30, "Guimarães"));

        PessoaAdapter pa = new PessoaAdapter(this, malta);

        ListView lvMalta = (ListView) findViewById(R.id.lvMalta);

        lvMalta.setAdapter(pa);

        lvMalta.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Pessoa p = malta.get(position);

                Toast.makeText(MainActivity.this, p.getNome(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
