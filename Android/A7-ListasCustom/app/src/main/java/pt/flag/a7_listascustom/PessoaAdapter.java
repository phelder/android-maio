package pt.flag.a7_listascustom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by helder on 22/06/16.
 */
public class PessoaAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Pessoa> malta;

    public PessoaAdapter(Context context, ArrayList<Pessoa> malta) {
        this.context = context;
        this.malta = malta;
    }

    @Override
    public int getCount() {
        return malta.size();
    }

    @Override
    public Object getItem(int position) {
        return malta.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.list_item_pessoa, parent, false);

        } else {

//            Toast.makeText(context, "RECICLADO", Toast.LENGTH_SHORT).show();

            v = convertView;

        }

        TextView tvNome = (TextView) v.findViewById(R.id.tvNome);
        TextView tvIdade = (TextView) v.findViewById(R.id.tvIdade);
        TextView tvCidade = (TextView) v.findViewById(R.id.tvCidade);

        Pessoa p = malta.get(position);

        tvNome.setText(p.getNome());
        tvIdade.setText(p.getIdade() + " anos");
        tvCidade.setText(p.getCidade());

        return v;
    }
}
