package pt.flag.a5_animacoes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.GridLayoutAnimationController;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvHelloWorld = (TextView) findViewById(R.id.tvHelloworld);
        Animation anim1 = AnimationUtils.loadAnimation(this, R.anim.anim1);

        anim1.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                Toast.makeText(MainActivity.this, "OHHHH", Toast.LENGTH_SHORT).show();
            }


        });

        tvHelloWorld.startAnimation(anim1);

    }
}
