package pt.flag.a1_intro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tvHelloWorld;
    private TextView tvQualquer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvHelloWorld = (TextView) findViewById(R.id.tvHelloWorld);
        tvQualquer = (TextView) findViewById(R.id.tvQualquer);

    }

    public void clickedButtonOK(View v) {

        Button clickedButton = (Button)v;

        clickedButton.setText("YAY JA CARREGASTE!!!!");

        tvHelloWorld.setText("Agora o texto é outro");
        tvQualquer.setText("Agora o texto é outro qualquer");
    }
}
