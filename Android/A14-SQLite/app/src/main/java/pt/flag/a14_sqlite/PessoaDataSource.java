package pt.flag.a14_sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by helder on 04/07/16.
 */
public class PessoaDataSource {

    private SQLiteDatabase db;

    public PessoaDataSource(SQLiteDatabase db) {
        this.db = db;
    }

    public long inserePessoa(String nome, int idade, String cidade) {

        ContentValues values = new ContentValues();
        values.put("nome", nome);
        values.put("idade", idade);
        values.put("cidade", cidade);

        long newId = db.insert("pessoas", null, values);

        return newId;

    }

    public void actualizaPessoa(int _id, String nome, int idade, String cidade) {

        ContentValues values = new ContentValues();
        values.put("nome", nome);
        values.put("idade", idade);
        values.put("cidade", cidade);

        String[] params = new String[1];
        params[0] = _id + "";

        db.update("pessoas", values, "_id = ?", params);

    }

    public void apagaPessoa(int _id) {

        db.delete("pessoas", "_id = " + _id, null);

    }

    public ArrayList<Pessoa> listaPessoas() {

        ArrayList<Pessoa> malta = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM pessoas", null);

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            String nome = cursor.getString(cursor.getColumnIndex("nome"));
            int idade = cursor.getInt(cursor.getColumnIndex("idade"));
            String cidade = cursor.getString(cursor.getColumnIndex("cidade"));

            Pessoa p = new Pessoa(nome, idade, cidade);

            malta.add(p);

            cursor.moveToNext();
        }

        cursor.close();

        return malta;

    }

    public Pessoa pessoaComId(int id) {

        Cursor cursor = db.rawQuery("SELECT * FROM pessoas WHERE _id = " + id, null);

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        String nome = cursor.getString(cursor.getColumnIndex("nome"));
        int idade = cursor.getInt(cursor.getColumnIndex("idade"));
        String cidade = cursor.getString(cursor.getColumnIndex("cidade"));

        Pessoa p = new Pessoa(nome, idade, cidade);

        cursor.close();

        return p;
    }
}
