package pt.flag.a14_sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by helder on 04/07/16.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    public MySQLiteHelper(Context context) {
        super(context, "my_db.sqlite", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTablePessoas = "CREATE TABLE pessoas (" +
                "_id INTEGER PRIMARY KEY, " +
                "nome TEXT, " +
                "idade INTEGER, " +
                "cidade TEXT)";

        db.execSQL(createTablePessoas);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }
}
