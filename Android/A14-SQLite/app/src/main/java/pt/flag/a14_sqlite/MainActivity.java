package pt.flag.a14_sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MySQLiteHelper mySQLiteHelper = new MySQLiteHelper(this);
        SQLiteDatabase db = mySQLiteHelper.getWritableDatabase();

        PessoaDataSource pds = new PessoaDataSource(db);

        pds.inserePessoa("Tobias", 12, "Lisboa");
        pds.inserePessoa("Natália", 2, "Porto");

        Toast.makeText(this, pds.listaPessoas().toString(), Toast.LENGTH_LONG).show();

        db.close();
        mySQLiteHelper.close();



    }
}
