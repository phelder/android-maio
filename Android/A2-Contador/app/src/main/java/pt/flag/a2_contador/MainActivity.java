package pt.flag.a2_contador;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RadioGroup rgContadores;
    private TextView tvContador1;
    private TextView tvContador2;

    private int contador1;
    private int contador2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rgContadores = (RadioGroup) findViewById(R.id.rgContadores);
        tvContador1 = (TextView) findViewById(R.id.tvContador1);
        tvContador2 = (TextView) findViewById(R.id.tvContador2);

        actualizaContadores();
    }

    public void clickedButton(View v) {

        if (v.getId() == R.id.btnMais) {

            if (rgContadores.getCheckedRadioButtonId() == R.id.rbContador1) {
                contador1++;
            } else {
                contador2++;
            }

        } else { // SO PODE SER O BOTAO - NÃO EXISTEM OUTROS

            if (rgContadores.getCheckedRadioButtonId() == R.id.rbContador1) {
                contador1--;
            } else {
                contador2--;
            }

        }

        actualizaContadores();
    }

    private void actualizaContadores() {

        tvContador1.setText("Contador 1: " + contador1);
        tvContador2.setText("Contador 2: " + contador2);

    };
}
