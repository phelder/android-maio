package pt.flag.a22_fragmentos;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout container = (FrameLayout) findViewById(R.id.container);

        if (container == null) {
            Toast.makeText(this, "ESTOU DEITADO", Toast.LENGTH_SHORT).show();
        } else {

            Toast.makeText(this, "ESTOU EM PE", Toast.LENGTH_SHORT).show();

            FirstFragment firstFragment = new FirstFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            ft.replace(R.id.container, firstFragment);

            ft.commit();

        }



    }
}
