package pt.flag.a20_internets;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvPosts = (ListView) findViewById(R.id.lvPosts);

        MyInternetTask mit = new MyInternetTask("http://jsonplaceholder.typicode.com/posts");

        mit.setOnConnectionListener(new MyInternetTask.OnConnectionListener() {
            @Override
            public void onConnectionSuccess(String result) {
                Toast.makeText(MainActivity.this, result, Toast.LENGTH_LONG).show();

                parseResult(result);
            }

            @Override
            public void onConnectionFailure() {
                Toast.makeText(MainActivity.this, "BOOOOOO", Toast.LENGTH_LONG).show();
            }
        });

        mit.execute();

    }

    private void parseResult(String result) {

        ArrayList<Post> posts = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(result);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject object = (JSONObject) jsonArray.get(i);

                Post p = new Post();
                p.setUserId(object.getInt("userId"));
                p.setId(object.getInt("id"));
                p.setTitle(object.getString("title"));
                p.setBody(object.getString("body"));

                posts.add(p);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        preencheListView(posts);
    }

    private void preencheListView(ArrayList<Post> posts) {

        ArrayAdapter<Post> adapter = new ArrayAdapter<Post>(this,
                android.R.layout.simple_list_item_1, posts);

        lvPosts.setAdapter(adapter);
    }
}
