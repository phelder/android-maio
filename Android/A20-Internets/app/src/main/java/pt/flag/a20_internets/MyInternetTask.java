package pt.flag.a20_internets;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 * Created by helder on 11/07/16.
 */
public class MyInternetTask extends AsyncTask <Void, Void, String> {

    public interface OnConnectionListener {
        void onConnectionSuccess(String result);
        void onConnectionFailure();
    }

    private String aUrl;

    public MyInternetTask(String aUrl) {
        this.aUrl = aUrl;
    }

    private OnConnectionListener listener;

    public void setOnConnectionListener(OnConnectionListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {

        StringBuilder result = null;

        try {
            URL url = new URL(this.aUrl);

            URLConnection connection = url.openConnection();

            Scanner input = new Scanner(connection.getInputStream());

            result = new StringBuilder();

            while (input.hasNextLine()) {
                result.append(input.nextLine());
            }

        } catch (Exception e) {

        }

        if (result != null) {
            return result.toString();
        } else {
            return null;
        }


    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if (listener != null) {
            if (s == null || s.length() == 0) {
                listener.onConnectionFailure();
            } else {
                listener.onConnectionSuccess(s);
            }
        }

    }
}
