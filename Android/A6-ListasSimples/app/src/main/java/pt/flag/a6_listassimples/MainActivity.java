package pt.flag.a6_listassimples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lvMalta = (ListView) findViewById(R.id.lvMalta);

//        ArrayList<String> nomes = new ArrayList<>();
//
//        nomes.add("Helder");
//        nomes.add("Tiago");
//        nomes.add("Vytman");
//        nomes.add("Diogo");
//        nomes.add("Bruno");
//        nomes.add("Nsimba");
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_list_item_1, nomes);
//
//        lvMalta.setAdapter(adapter);

        final ArrayList<Pessoa> malta = new ArrayList<>();

        malta.add(new Pessoa("Tobias", 20, "Porto"));
        malta.add(new Pessoa("Tomé", 21, "Lisboa"));
        malta.add(new Pessoa("Zeca", 22, "Lisboa"));
        malta.add(new Pessoa("Boné", 40, "Porto"));
        malta.add(new Pessoa("Ruben", 30, "Aveiro"));
        malta.add(new Pessoa("Popeye", 25, "Braga"));
        malta.add(new Pessoa("Margarida", 19, "Coimbra"));
        malta.add(new Pessoa("Sofia", 30, "Porto"));

        ArrayAdapter<Pessoa> pessoaArrayAdapter =
                new ArrayAdapter<Pessoa>(this, R.layout.list_item_pessoa, malta);


        lvMalta.setAdapter(pessoaArrayAdapter);

        lvMalta.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(MainActivity.this, "TEXTO QQ", Toast.LENGTH_SHORT).show();
                //Toast.makeText(MainActivity.this, position + "", Toast.LENGTH_SHORT).show();

//                TextView tvItem = (TextView)view;
//                Toast.makeText(MainActivity.this, tvItem.getText(), Toast.LENGTH_SHORT).show();

//                Pessoa p = malta.get(position);
//                String infoP = p.getNome() + " - " + p.getIdade() + " - " + p.getCidade();
//                Toast.makeText(MainActivity.this, infoP, Toast.LENGTH_SHORT).show();

                Pessoa p = (Pessoa)parent.getItemAtPosition(position);
                String infoP = p.getNome() + " - " + p.getIdade() + " - " + p.getCidade();
                Toast.makeText(MainActivity.this, infoP, Toast.LENGTH_SHORT).show();
            }

        });
    }
}
