package pt.flag.a12_sharedpreferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText etTexto = (EditText) findViewById(R.id.etTexto);

        final SharedPreferences pref = getSharedPreferences("pref1", MODE_PRIVATE);

        String texto = pref.getString("texto", "");

        etTexto.setText(texto);

        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor edit = pref.edit();

                edit.putString("texto", etTexto.getText().toString());

                edit.commit();
            }
        });
    }



    // EXEMPLOS...
    private void escrever() {
        SharedPreferences pref = getSharedPreferences("pref1", MODE_PRIVATE);

        SharedPreferences.Editor edit = pref.edit();

        edit.putString("nome", "Helder");
        edit.putInt("numero_porta", 57);

        edit.commit();
    }

    private void ler() {
        SharedPreferences pref = getSharedPreferences("pref1", MODE_PRIVATE);

        String n = pref.getString("nome", "");
        int p = pref.getInt("numero_porta", 0);

    }
}
