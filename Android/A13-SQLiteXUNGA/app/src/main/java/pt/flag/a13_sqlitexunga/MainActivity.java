package pt.flag.a13_sqlitexunga;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteDatabase db = openOrCreateDatabase("my_db.sqlite", MODE_PRIVATE, null);

        String createTable = "CREATE TABLE IF NOT EXISTS pessoas (" +
                "_id INTEGER PRIMARY KEY, " +
                "nome TEXT, " +
                "idade INTEGER, " +
                "cidade TEXT" +
                ")";

        db.execSQL(createTable);

        // INSERIR

        String insert = "INSERT INTO pessoas (nome, idade, cidade) VALUES('Tomé', 12, 'Porto')";

        db.execSQL(insert);


        // LER

        Cursor cursor = db.rawQuery("SELECT * FROM pessoas", null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {



            int id = cursor.getInt(0);
            String nome = cursor.getString(cursor.getColumnIndex("nome"));
            cursor.getInt(2);
            cursor.getString(3);

            cursor.moveToNext();

            Toast.makeText(this, id + " - " + nome, Toast.LENGTH_SHORT).show();
        }

    }
}
