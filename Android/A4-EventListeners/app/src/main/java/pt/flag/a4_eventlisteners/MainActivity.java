package pt.flag.a4_eventlisteners;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnHelloWorld = (Button) findViewById(R.id.btnHelloWorld);
        Button btnGoodbyeWorld = (Button) findViewById(R.id.btnGoodbyeWorld);

        Button btnGo = (Button) findViewById(R.id.btnGO);
        final EditText etShowText = (EditText) findViewById(R.id.etShowText);

        btnHelloWorld.setOnClickListener(this);
        btnGoodbyeWorld.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Toast.makeText(MainActivity.this, "GOODBYE WORLD!!!!", Toast.LENGTH_LONG).show();

            }
        });

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String texto = etShowText.getText().toString();

                Toast.makeText(MainActivity.this, texto, Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "HELLO WORLD", Toast.LENGTH_SHORT).show();
    }
}
