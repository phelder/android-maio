package com.company;

public class Main {

    public static void main(String[] args) {

        Pessoa[] malta = new Pessoa[6];

        malta[0] = new Pessoa();
        malta[0].nome = "Helder";
        malta[0].idade = 35;
        malta[0].cidade = "Porto";

        malta[1] = new Pessoa();
        malta[1].nome = "Diogo";
        malta[1].idade = 22;
        malta[1].cidade = "Leça do Balio";

        malta[2] = new Pessoa();
        malta[2].nome = "Vítor";
        malta[2].idade = 32;
        malta[2].cidade = "Trofa";

        malta[3] = new Pessoa();
        malta[3].nome = "Tiago";
        malta[3].idade = 31;
        malta[3].cidade = "Gaia";

        malta[4] = new Pessoa();
        malta[4].nome = "Bruno";
        malta[4].idade = 22;
        malta[4].cidade = "Gaia";

        malta[5] = new Pessoa();
        malta[5].nome = "Pedro";
        malta[5].idade = 22;
        malta[5].cidade = "Porto";

        int posMaisVelho = 0;
        Pessoa maisVelho = malta[0];
        Pessoa maisNovo = malta[0];

        int contadorMV = 0;
        int contadorMN = 0;

        int soma = 0;

        for (int i = 0; i < malta.length; i++) {
//            System.out.println(malta[i].nome + " - " + malta[i].idade);

            if (malta[i].idade > malta[posMaisVelho].idade) {
                posMaisVelho = i;
            }

            if (malta[i].idade > maisVelho.idade) {
                maisVelho = malta[i];
                contadorMV = 1;
            } else if(malta[i].idade == maisVelho.idade) {
                contadorMV++;
            }

            if (malta[i].idade < maisNovo.idade) {
                maisNovo = malta[i];
                contadorMN = 1;
            } else if(malta[i].idade == maisNovo.idade) {
                contadorMN++;
            }

            soma += malta[i].idade;

        }

        // TPC

        System.out.println("Mais velho: " + malta[posMaisVelho].nome);
        System.out.println("Mais velho: " + maisVelho.nome);
        System.out.println("Mais novo: " + maisNovo.nome);

        System.out.println("Média: " + (float)soma / malta.length);

        // IMPRIMIR NOME, IDADE E CIDADE DO MAIS VELHO...
        // IMPRIMIR NOME, IDADE E CIDADE DO MAIS NOVO...
        // IMPRIMIR MÉDIA DE IDADES

    }
}
