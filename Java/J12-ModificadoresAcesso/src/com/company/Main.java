package com.company;

public class Main {

    public static void main(String[] args) {

        Pessoa p1 = new Pessoa();
        p1.setNome("Helder");
        p1.setCidade("Porto");
        p1.setIdade(35);

        Pessoa p2 = new Pessoa();
        p2.setNome("Tobias");
        p2.setCidade("Lisboa");
        p2.setIdade(45);

        if (p1.getIdade() > p2.getIdade()) {

            System.out.println("O mais velho é o " + p1.getNome());

        } else {

            System.out.println("O mais velho não é o " + p1.getNome());
        }

    }
}
