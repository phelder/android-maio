package com.company;

/**
 * Created by helder on 01/06/16.
 */
public class AdivinhaNumero {

    public enum Resultado {
        MAIOR,
        MENOR,
        IGUAL
    }

    private int numeroRandom;
    private int vidas;

    public AdivinhaNumero() {
        numeroRandom = randomiza(1, 100);
        vidas = 8;
    }

    public AdivinhaNumero(int vidas) {
        numeroRandom = randomiza(1, 100);
        this.vidas = vidas;
    }

    public AdivinhaNumero(int min, int max) {
        numeroRandom = randomiza(min, max);
        vidas = 8;
    }

    public AdivinhaNumero(int vidas, int min, int max) {
        numeroRandom = randomiza(min, max);
        this.vidas = vidas;
    }

    public int getVidas() {
        return vidas;
    }

    public boolean emJogo() {

        if (vidas > 0) {
            return true;
        }
        return false;
    }

    public Resultado verificaNumero(int numero) {

        if (numeroRandom == numero) {

            return Resultado.IGUAL;

        } else if (numeroRandom > numero) {

            vidas--;

            return Resultado.MAIOR;

        } else { // SO PODE SER MENOR...

            vidas--;

            return Resultado.MENOR;

        }
    }

    public static int randomiza(int min, int max) {
        return (int)(Math.random() * (max + 1 - min)) + min;
    }

}
