package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        AdivinhaNumero anJogo = new AdivinhaNumero(10, 1, 200);

        while (anJogo.emJogo()) {

            System.out.println("Vidas: " + anJogo.getVidas());
            System.out.println("Introduza um número");

            int numero = input.nextInt();

            AdivinhaNumero.Resultado resposta = anJogo.verificaNumero(numero);

            if (resposta == AdivinhaNumero.Resultado.IGUAL) {
                break;
            }

            if (resposta == AdivinhaNumero.Resultado.MAIOR) {
                System.out.println("Tens de subir");
            } else if (resposta == AdivinhaNumero.Resultado.MENOR) {
                System.out.println("Tens de descer");
            }
        }

        if (anJogo.getVidas() > 0) {
            System.out.println("Acertaste!!!!");
        } else {
            System.out.println("NABO!!!!!!!!!");
        }

    }
}
