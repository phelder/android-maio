package com.company;

/**
 * Created by helder on 13/06/16.
 */
public class UmaClasse implements OMeuInterface {

    @Override
    public void oMeuMetodo() {

    }

    @Override
    public String oMeuOutroMetodo() {
        return null;
    }

    @Override
    public void oMeuMetodoComArgumento(int a, int b) {

    }
}
