package com.company;

/**
 * Created by helder on 13/06/16.
 */
public class Vaca extends Animal {

    public Vaca() {
        super(4);
    }

    @Override
    public String fazSom() {
        return "MOOOOO";
    }
}
