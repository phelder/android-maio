package com.company;

/**
 * Created by helder on 13/06/16.
 */
public interface OMeuInterface {

    void oMeuMetodo();
    String oMeuOutroMetodo();
    void oMeuMetodoComArgumento(int a, int b);

}
