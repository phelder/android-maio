package com.company;

/**
 * Created by helder on 13/06/16.
 */
public interface OnClickListener {

    void onClick(Object v);

}
