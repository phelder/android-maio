package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Animal> animais = new ArrayList<>();

        animais.add(new Cao());
        animais.add(new Cao());
        animais.add(new Gato());
        animais.add(new Vaca());
        animais.add(new Animal(5) {

            @Override
            public String fazSom() {

                return "WHOOPS!!!!";
            }

            @Override
            public String toString() {
                return "HEY EU NAO SEI O QUE SOU...";
            }
        });

        System.out.println(animais);
    }
}
