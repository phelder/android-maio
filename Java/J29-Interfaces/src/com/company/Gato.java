package com.company;

/**
 * Created by helder on 13/06/16.
 */
public class Gato extends Animal {

    public Gato() {
        super(4);
    }

    @Override
    public String fazSom() {
        return "MIAUUUU!!!!!";
    }
}
