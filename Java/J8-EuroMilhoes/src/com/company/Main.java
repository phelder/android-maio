package com.company;

public class Main {

    public static void main(String[] args) {

        // CRIAR ARRAYS VAZIOS
        int[] numeros = new int[5];
        int[] estrelas = new int[2];

        // PREENCHER ARRAY NUMEROS
        for (int i = 0; i < numeros.length; i++) {

            int numero;
            do {
                numero = randomiza(1, 50);

            } while (jaExiste(numero, numeros));

            numeros[i] = numero;
        }

        // PREENCHER ARRAY ESTRELAS
        for (int i = 0; i < estrelas.length; i++) {

            int numero;
            do {
                numero = randomiza(1, 11);
            } while(jaExiste(numero, estrelas));

            estrelas[i] = numero;
        }

        // IMPRIMIR NUMEROS
        for (int i = 0; i < numeros.length; i++) {
            System.out.println(numeros[i]);
        }

        // IMPRIMIR ESTRELAS
        for (int i = 0; i < estrelas.length; i++) {
            System.out.println(estrelas[i]);
        }

    }

    public static int randomiza(int min, int max) {
        return (int)(Math.random() * (max + 1 - min)) + min;
    }

    public static boolean jaExiste(int agulha, int[] palheiro) {

        for (int i = 0; i < palheiro.length; i++) {
            if (agulha == palheiro[i]) {
                return true;
            }
        }
        return false;
    }
}
