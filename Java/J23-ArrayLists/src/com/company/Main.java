package com.company;

import java.util.ArrayList;

public class Main {

    public static boolean primo(int numero) {
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
//        Classes wrapper para primitivas
//        Integer;
//        Long;
//        Character;
//        Boolean;
//        Double;
//        Float;

        ArrayList<Long> fibo = new ArrayList<>();

        fibo.add(0L);
        fibo.add(1L);

        for (int i = 2; i < 50; i++) {

            fibo.add(fibo.get(i - 1) + fibo.get(i - 2));

        }

        System.out.println(fibo);


        ArrayList<Integer> primos = new ArrayList<>();
        ArrayList<Integer> posSaltosMaisAltos = new ArrayList<>();
        int saltoMaisAlto = 0;

        int pos = -1;
        for (int i = 1; i < 2000000; i++) {

            if (primo(i)) {
                primos.add(i);
                pos++;
            } else {
                continue;
            }

            if (pos > 0) {
                int salto = primos.get(pos) - primos.get(pos - 1);

                if (salto > saltoMaisAlto) {
                    posSaltosMaisAltos.clear();
                    saltoMaisAlto = salto;
                }

                if (salto == saltoMaisAlto) {
                    posSaltosMaisAltos.add(pos - 1);
                }

            }
        }

        System.out.println(primos.size());

        System.out.println(posSaltosMaisAltos);
        System.out.println(saltoMaisAlto);

        for (int i = 0; i < posSaltosMaisAltos.size(); i++) {

            System.out.println(primos.get(posSaltosMaisAltos.get(i)));
            System.out.println(primos.get(posSaltosMaisAltos.get(i) + 1));
            System.out.println("-----");

        }

    }
}
