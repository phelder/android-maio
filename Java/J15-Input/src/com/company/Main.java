package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Introduza um número");
        int numero = input.nextInt();

        System.out.println("Introduza outro número");
        int outroNumero = input.nextInt();

        int soma = numero + outroNumero;

        System.out.println("Soma: " + soma);
    }
}
