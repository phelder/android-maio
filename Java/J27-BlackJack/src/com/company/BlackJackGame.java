package com.company;

import java.util.ArrayList;

/**
 * Created by helder on 08/06/16.
 */
public class BlackJackGame {


    private ArrayList<BlackJackPlayer> players;
    private ArrayList<BlackJackCard> deck;
    private BlackJackPlayer currentPlayer;

    public BlackJackGame(boolean dealer, int numPlayers) {
        players = new ArrayList<>();

        if (dealer) {
            players.add(new BlackJackDealer());
            numPlayers--;
        }

        for (int i = 0; i < numPlayers; i++) {
            players.add(new BlackJackPlayer("P" + ( i + 1)));
        }
        initGame();
    }

    public BlackJackGame (boolean dealer, String... playerNames) {
        players = new ArrayList<>();

        for (int i = 0; i < playerNames.length; i++) {

            if (i == 0 && dealer) {
                players.add(new BlackJackDealer(playerNames[i]));
            } else {
                players.add(new BlackJackPlayer(playerNames[i]));
            }



        }
        initGame();
    }

    private void initGame() {
        generateNewDeck();

        currentPlayer = players.get(0);

        for (int i = 0; i < players.size(); i++) {
            players.get(i).addCard(getCard());
            players.get(i).addCard(getCard());
        }
    }

    private void generateNewDeck() {

        deck = new ArrayList<>();

        for (int i = 0; i < BlackJackCard.values().length; i++) {
            for (int j = 0; j < 4; j++) {
                deck.add(BlackJackCard.values()[i]);
            }
        }
    }

    public BlackJackCard getCard() {

        int pos = randomiza(0, deck.size() -1);

        BlackJackCard card = deck.get(pos);

        deck.remove(pos);

        return card;
    }

    public void moveToNextPlayer() {

        int pos = players.indexOf(currentPlayer);

        pos++;
        if (pos >= players.size()) {
            pos = 0;
        }

        currentPlayer = players.get(pos);
    }

    public BlackJackPlayer getCurrentPlayer() {
        return currentPlayer;
    }

    public static int randomiza(int min, int max) {
        return (int)(Math.random() * (max + 1 - min)) + min;
    }

    public ArrayList<BlackJackPlayer> getPlayers() {
        return players;
    }
}
