package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        Scanner input = new Scanner(System.in);
//
//        BlackJackPlayer p1 = new BlackJackPlayer("P1");
//
//        BlackJackGame game = new BlackJackGame();
//
//        p1.addCard(game.getCard());
//        p1.addCard(game.getCard());
//
//        System.out.println(p1.getHand());
//
//        for (int i = 0; i < p1.getHand().length; i++) {
//            System.out.println(p1.getHand()[i]);
//        }
//        System.out.println(p1.handValue());
//
//        while (p1.handValue() < 21) {
//
//            System.out.println("MAIS UMA? (s/n)");
//            String resposta = input.nextLine();
//
//            if (resposta.charAt(0) == 's') {
//                p1.addCard(game.getCard());
//                for (int i = 0; i < p1.getHand().length; i++) {
//                    System.out.println(p1.getHand()[i]);
//                }
//                System.out.println(p1.handValue());
//            } else {
//                break;
//            }
//        }
//
//
//
//        for (int i = 0; i < p1.getHand().length; i++) {
//            System.out.println(p1.getHand()[i]);
//        }
//        System.out.println(p1.handValue());
//
//        if (p1.handValue() >= 19 && p1.handValue() <= 21) {
//            System.out.println("YAY");
//        } else {
//            System.out.println("BOOO");
//        }

        Scanner input = new Scanner(System.in);

        System.out.println("Quantos jogadores?");
        int numJogadores = input.nextInt();
        // limpar o buffer
        if (input.hasNextLine()) {
            input.nextLine();
        }

        String[] nomes = new String[numJogadores];

        for (int i = 0; i < numJogadores; i++) {
            System.out.println("Qual o nome do " + (i + 1) + "º?");
            String nome = input.nextLine();
            nomes[i] = nome;
        }


        BlackJackGame game = new BlackJackGame(true, nomes);


//        for (BlackJackPlayer p : game.getPlayers()) {
//
//            System.out.println(p.getName());
//            System.out.println(p.getHandString());
//            System.out.println("-----------");
//        }

        ArrayList<BlackJackPlayer> vencedores = new ArrayList<>();

        int ronda;
        for (int i = 0; i < 4; i++) {
            ronda = i + 1;

            for (int j = 0; j < game.getPlayers().size(); j++) {
                BlackJackPlayer p = game.getCurrentPlayer();

                if (p.handValue() > 21) {
                    game.moveToNextPlayer();
                    continue;
                }

                System.out.println("Ronda: " + ronda);
                System.out.println("J: " + p.getName());
                System.out.println(p.getHandString() + ": " + p.handValue());
                System.out.println("Ó " + p.getName() + " queres mais uma? (s/n)");

                if (p instanceof BlackJackDealer) {

                    BlackJackDealer d = (BlackJackDealer)p;
                    if (d.shouldGetCard()) {
                        d.addCard(game.getCard());
                    }

                } else {
                    char resposta = input.nextLine().charAt(0);

                    if (resposta == 's' || resposta == 'S') {
                        p.addCard(game.getCard());
                        System.out.println(p.getHandString() + ": " + p.handValue());
                    }
                }



                if (p.handValue() == 21) {
                    vencedores.add(p);
                }

                System.out.println("-----------");
                game.moveToNextPlayer();
            }

            if (vencedores.size() > 0) {
                break;
            }
        }


        if (vencedores.size() == 0) { // NINGUEM FEZ 21 CERTOS...
            int maior = 0;
            for (BlackJackPlayer p : game.getPlayers()) {
                if (p.handValue() > maior && p.handValue() <= 21) {
                    vencedores.clear();
                    maior = p.handValue();
                }

                if (p.handValue() == maior) {
                    vencedores.add(p);
                }
            }
        }

        System.out.println("V: " + vencedores);

    }

}
