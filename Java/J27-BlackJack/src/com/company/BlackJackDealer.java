package com.company;

/**
 * Created by helder on 13/06/16.
 */
public class BlackJackDealer extends BlackJackPlayer {

    public BlackJackDealer() {
        super("Mr. Dealer");
    }

    public BlackJackDealer(String name) {
        super(name);
    }

    public boolean shouldGetCard() {
        if (this.handValue() < 18) {
            return true;
        }
        return false;
    }
}
