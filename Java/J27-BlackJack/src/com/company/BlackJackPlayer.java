package com.company;

import java.util.ArrayList;

/**
 * Created by helder on 08/06/16.
 */
public class BlackJackPlayer {

    private ArrayList<BlackJackCard> hand;
    private String name;

    public BlackJackPlayer(String name) {

        hand = new ArrayList<>();

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addCard(BlackJackCard card) {
        hand.add(card);
    }

    public Object[] getHand() {
        return hand.toArray();
    }

    public String getHandString() {

        String result = "";
        for (int i = 0; i < hand.size(); i++) {
            if (i > 0) {
                result += ", ";
            }
            result += hand.get(i).toString();
        }
        return result;
    }

    public int handValue() {

        int sum = 0;
        int aceCounter = 0;

        for (int i = 0; i < hand.size(); i++) {

            if (hand.get(i) == BlackJackCard.ACE) {
                aceCounter++;
            } else {
                sum += hand.get(i).getValue();
            }
        }

        for (int i = 0; i < aceCounter; i++) {

            if (sum + BlackJackCard.ACE.getValueB() > 21) {
                sum += BlackJackCard.ACE.getValue();
            } else {
                sum += BlackJackCard.ACE.getValueB();
            }
        }

        return sum;
    }

    @Override
    public String toString() {
        return name + " - " + getHandString();
    }
}
