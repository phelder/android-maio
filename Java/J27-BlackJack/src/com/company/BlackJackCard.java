package com.company;

/**
 * Created by helder on 08/06/16.
 */
public enum BlackJackCard {

    ACE(1, 11),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(10),
    QUEEN(10),
    KING(10);

    private int value;
    private int valueB;

    BlackJackCard(int value) {
        this.value = value;
    }

    BlackJackCard(int value, int valueB) {
        this.value = value;
        this.valueB = valueB;
    }

    public int getValue() {
        return value;
    }

    public int getValueB() {
        return valueB;
    }

    @Override
    public String toString() {

        String result;

        switch (this) {
            case ACE:
                result = "A";
                break;
            case JACK:
                result = "J";
                break;
            case QUEEN:
                result = "Q";
                break;
            case KING:
                result = "K";
                break;
            default:
                result = String.valueOf(this.getValue());
                break;
        }
        return result;
    }
}
