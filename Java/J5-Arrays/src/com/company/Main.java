package com.company;

public class Main {

    public static void main(String[] args) {

        int[] idades = new int[5];

        idades[0] = 35;
        idades[1] = 22;
        idades[2] = 32;
        idades[3] = 31;
        idades[4] = 22;

        for (int i = 0; i < idades.length; i++) {

            System.out.println(":" + (i + 1) + ": " + idades[i]);

        }

        System.out.println("PRIMEIROS 10 PRIMOS:");

        int[] primos = new int[10];

        int numero = 0;
        int contador = 0;

        while (contador < 10) {

            numero ++;

            boolean primo = true;
            for (int i = 2; i <= Math.sqrt(numero); i++) {
                if (numero % i == 0) {
                    primo = false;
                    break;
                }
            }

            if (primo) {
                primos[contador] = numero;
                contador++;
            }
        }

        for (int i = 0; i < 10; i++) {
            System.out.println((i + 1) + ": " + primos[i]);
        }

    }
}
