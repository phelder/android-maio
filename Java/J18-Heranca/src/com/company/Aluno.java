package com.company;

/**
 * Created by helder on 01/06/16.
 */
public class Aluno extends Pessoa {

    private String turma;
    private int nota;

    public Aluno(String nome, int idade, String cidade, String turma, int nota) {
        super(nome, idade, cidade);
        this.turma = turma;
        this.nota = nota;
    }

    public Aluno(String turma, int nota) {
        this.turma = turma;
        this.nota = nota;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
}
