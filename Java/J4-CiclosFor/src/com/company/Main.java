package com.company;

public class Main {

    public static void main(String[] args) {

        for (int i = 1; i <= 10; i++) {

            for (int j = 1; j <= 10; j++) {

                System.out.println(i + " X " + j + " = " + (i*j));

            }
            System.out.println("");
        }

        int n1 = 0;
        int n2 = 1;

        System.out.println("1: " + n1);
        System.out.println("2: " + n2);

        for (int i = 3; i <= 20; i++) {

            int temp = n1 + n2;
            System.out.println(i + ": " + temp);

            n1 = n2;
            n2 = temp;
        }

    }
}
