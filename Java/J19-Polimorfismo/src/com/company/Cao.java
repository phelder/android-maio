package com.company;

/**
 * Created by helder on 01/06/16.
 */
public class Cao extends Animal {

    public Cao() {
        super(4);
    }

    public String ladra() {
        return "AU AU!!!!";
    }

    public String rosna() {
        return "GRRRRRRRR!!!!";
    }

    @Override
    public String fazSom() {
        return ladra();
    }
}
