package com.company;

/**
 * Created by helder on 01/06/16.
 */
public class Pato extends Animal {

    public Pato () {
        super(2);
    }

    public String grasna() {
        return "QUA QUA";
    }

    @Override
    public String fazSom() {

        String original = super.fazSom();

        return original + " " + grasna();
    }


}
