package com.company;

/**
 * Created by helder on 01/06/16.
 */
public abstract class Animal {

    private int numeroPatas;

    public Animal(int numeroPatas) {
        this.numeroPatas = numeroPatas;
    }

    public int getNumeroPatas() {
        return numeroPatas;
    }

    public String fazSom() {
        return "Som por defeito do animal...";
    }

}
