package com.company;

import java.util.ArrayList;

public class Main {


    public static void main(String[] args) {

        teste();

        try {
            outroTeste();
        } catch (PessoaSemNomeException e) {
            e.printStackTrace();
        }

        System.out.println("EU JA APARECO...");

    }

    public static void teste() {
        Pessoa p1 = new Pessoa("", 12, "Porto");

        try {

            p1.getNome();

        } catch (PessoaSemNomeException e) {
            System.out.println("ESTE GAJO NAO TEM NOME...");
        }
    }

    public static void outroTeste() throws PessoaSemNomeException {

        Pessoa p1 = new Pessoa("", 12, "Porto");

        p1.getNome();
    }
}
