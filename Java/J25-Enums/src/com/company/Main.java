package com.company;

public class Main {


    public static void main(String[] args) {


        PontosCardeais sentido = PontosCardeais.SUL;

        switch (sentido) {
            case NORTE:
                System.out.println("VAI PARA CIMA");
                break;
            case SUL:
                System.out.println("VAI PARA BAIXO");
                break;
            case ESTE:
                System.out.println("VAI PARA O LADO");
                break;
            case OESTE:
                System.out.println("VAI PARA O OUTRO LADO");
                break;
        }

    }
}
