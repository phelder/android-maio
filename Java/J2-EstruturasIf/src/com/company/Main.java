package com.company;

public class Main {

    public static void main(String[] args) {

        // OPERADORES CONDICIONAIS
        // ==
        // !=
        // >
        // >=
        // <
        // <=

        int idade = 10;

        if (idade >= 18) {

            System.out.println("Toma lá uma cerveja!");

        } else {

            System.out.println("Toma lá uma cola!");

        }

        int a = 34;
        int b = 12;
        int c = 54;

        if (a > b) {

            if (a > c) {
                System.out.println("Maior: " + a);

            } else {
                System.out.println("Maior: " + c);
            }

        } else {

            if (b > c) {
                System.out.println("Maior: " + b);
            } else {
                System.out.println("Maior: " + c);
            }

        }
    }
}
