package com.company;

import java.util.Scanner;

public class Main {

    public static int randomiza(int min, int max) {
        return (int)(Math.random() * (max + 1 - min)) + min;
    }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int numeroRandom = randomiza(1, 100);
        // BATOTA
        System.out.println(numeroRandom);

        int numeroAdivinha;

        int vidas = 8;

        do {
            System.out.println("Tentativas " + vidas);
            System.out.println("Introduza um número: ");
            numeroAdivinha = input.nextInt();

            if (numeroRandom == numeroAdivinha) {
                break;
            }

            if (numeroRandom > numeroAdivinha) {
                System.out.println("Tens de subir");
            } else {
                System.out.println("Tens de descer");
            }
            vidas--;

        } while(vidas > 0);


        if (vidas > 0) {
            System.out.println("ACERTASTE!!!!");
        } else {
            System.out.println("NABO!!!!");
        }
    }
}
