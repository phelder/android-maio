package com.company;

public class Main {

    public static void main(String[] args) {

        int numTabuada = 0;

        while (numTabuada < 10) {
            numTabuada++;

            int num = 0;
            while (num < 10) {
                num++;

                int res = numTabuada * num;

                System.out.println(numTabuada + " X " + num + " = " + res);
            }

            System.out.println("");
        }

    }
}
