package com.company;

public class Main {

    public static void main(String[] args) {

        // AS STRINGS SAO IMUTAVEIS

        String text = new String("Pokemon");
        String outroApontador = text;


        String newText = text.substring(3, 5);

        System.out.println(text);
        System.out.println(newText);

        text += "!!!!!!!!!!";

        System.out.println(text);
        System.out.println(outroApontador);


        // COMPARAR STRINGS

        String a = "TO";
        a += "bias";
        String b = "Tobias";

        // NUNCA USAR ==
        // PORQUE APENAS COMPARA O VALOR DO APONTADOR NA STACK
        if (a.equalsIgnoreCase(b)) {

            System.out.println("As Strings sao iguais!");

        } else {

            System.out.println("As Strings sao diferentes!");

        }



    }
}
