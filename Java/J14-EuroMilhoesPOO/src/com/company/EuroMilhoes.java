package com.company;

import com.sun.deploy.util.StringUtils;

import java.util.Arrays;

/**
 * Created by helder on 30/05/16.
 */
public class EuroMilhoes {

    private static final int NUM_NUMEROS = 5;
    private static final int NUM_ESTRELAS = 2;

    private static final int MIN_NUMEROS = 1;
    private static final int MAX_NUMEROS = 50;

    private static final int MIN_ESTRELAS = 1;
    private static final int MAX_ESTRELAS = 11;

    private int[] numeros = new int[NUM_NUMEROS];
    private int[] estrelas = new int[NUM_ESTRELAS];

    public EuroMilhoes() {
        geraChave();
    }

    private void geraChave() {
        // aqui vamos preencher os arrays...

        // PREENCHER ARRAY NUMEROS
        for (int i = 0; i < numeros.length; i++) {

            int numero;
            do {
                numero = randomiza(MIN_NUMEROS, MAX_NUMEROS);

            } while (jaExiste(numero, numeros));

            numeros[i] = numero;
        }

        // PREENCHER ARRAY ESTRELAS
        for (int i = 0; i < estrelas.length; i++) {

            int numero;
            do {
                numero = randomiza(MIN_ESTRELAS, MAX_ESTRELAS);
            } while(jaExiste(numero, estrelas));

            estrelas[i] = numero;
        }

        Arrays.sort(numeros);
        Arrays.sort(estrelas);
    }

    public int[] getNumeros() {
        return numeros;
    }

    public int[] getEstrelas() {
        return estrelas;
    }

    public String getNumerosString() { // EX: 2, 4, 20, 31, 50

        String numerosString = "";

        for (int i = 0; i < numeros.length; i++) {

            if (i != 0) {
                numerosString += ", ";
            }
            numerosString += numeros[i];
        }
        return numerosString;
    }

    public String getEstrelasString() { // EX: 3, 10

        String estrelasString = "";

        for (int i = 0; i < estrelas.length; i++) {

            if (i != 0) {
                estrelasString += ", ";
            }
            estrelasString += estrelas[i];
        }
        return estrelasString;
    }

    public String getChave() { // EX: 2, 4, 20, 31, 50 - 3, 10

        return getNumerosString() + " - " + getEstrelasString();

    }

    public static int randomiza(int min, int max) {
        return (int)(Math.random() * (max + 1 - min)) + min;
    }

    public static boolean jaExiste(int agulha, int[] palheiro) {

        for (int i = 0; i < palheiro.length; i++) {
            if (agulha == palheiro[i]) {
                return true;
            }
        }
        return false;
    }
}
