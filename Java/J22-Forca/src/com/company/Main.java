package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String[] frases = {
                "Gotham City",
                "Metropolis",
                "Star City",
                "Coast City",
                "Atlantis",
                "New Themyscira",
                "Central City"
        };

        Scanner input = new Scanner(System.in);

        Forca f1 = new Forca(frases);

        while (f1.emJogo()) {
            // IMPRIMIR INFO
            System.out.println("vidas: " + f1.getVidas());
            System.out.println(f1.getFraseJogo());

            // PEDIMOS UMA LETRA UTILIZADOR
            System.out.println("Letra?");
            String letra = input.next();
            char l = letra.charAt(0);

            // VERIFICAMOS A LETRA
            f1.verificaLetra(l);

        }


        if (f1.ganhou()) {
            System.out.println(f1.getFraseJogo());
            System.out.println("WEEEEE!!!!");
        } else {
            System.out.println("NABO!!!!");
        }

    }
}
