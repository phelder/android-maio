package com.company;

import java.text.Collator;

/**
 * Created by helder on 03/06/16.
 */
public class Forca {

    private String fraseSecreta;
    private StringBuilder fraseJogo;

    private int vidas;

    public Forca() {
        vidas = 6;
        fraseSecreta = "pika pika pikachu";
        iniciaJogo();
    }

    public Forca(String fraseSecreta) {
        vidas = 6;
        this.fraseSecreta = fraseSecreta;
        iniciaJogo();
    }

    public Forca(String[] frasesSecretas) {
        vidas = 6;

        int pos = (int)(Math.random() * frasesSecretas.length);
        this.fraseSecreta = frasesSecretas[pos];
        iniciaJogo();
    }

    private void iniciaJogo() {

        fraseJogo = new StringBuilder();

        for (int i = 0; i < fraseSecreta.length(); i++) {
            if (fraseSecreta.charAt(i) == ' ') {
                fraseJogo.append(' ');
            } else {
                fraseJogo.append('-');
            }
        }
    }

//    boolean verificaLetra(char letra) {
//
//        boolean existe = false;
//
//        for (int i = 0; i < fraseSecreta.length(); i++) {
//            if (fraseSecreta.charAt(i) == letra) {
//                fraseJogo.setCharAt(i, letra);
//                existe = true;
//            }
//        }
//
//        if (!existe) {
//            vidas--;
//        }
//
//        return existe;
//    }

    boolean verificaLetra(char letra) {

        boolean existe = false;

        Collator collator = Collator.getInstance();
        collator.setStrength(Collator.PRIMARY);

        String letraUtilizador = String.valueOf(letra);

        for (int i = 0; i < fraseSecreta.length(); i++) {

            String letraFrase = String.valueOf(fraseSecreta.charAt(i));

            if (collator.compare(letraFrase, letraUtilizador) == 0) {

                fraseJogo.setCharAt(i, fraseSecreta.charAt(i));

                existe = true;
            }

        }

        if (!existe) {
            vidas--;
        }

        return existe;
    }


    boolean emJogo() {
        return !fraseSecreta.equals(fraseJogo.toString()) && vidas > 0;
    }

    boolean ganhou() {
        return !emJogo() && vidas > 0;
    }

    public String getFraseJogo() {
        return fraseJogo.toString();
    }

    public int getVidas() {
        return vidas;
    }

    @Override
    public String toString() {
        return "OLA OLA OLA EU SOU UMA FORCA!!!!";
    }
}
